---
---
# Welcome

[[toc]]

::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger Custom Danger
This is a dangerous warning
:::

``` js{4}
export default {
  data () {
    return {
      msg: 'Highlighted!'
    }
  }
}
```

``` bash
export MSG="END OF EXAMPLE"
echo $MSG
```

Welcome to the Big Data Bootcamp. This training material is has been developed by [Sunlab](http://www.sunlab.org/). By the end of the training, you will learn about the big data tools that are part of the [Hadoop](http://hadoop.apache.org) and [Spark](http://spark.apache.org) ecosystems.

The training material [sample data](data) is for healthcare applications, but you can adapt what you learned to other domains. There is no requirement of healthcare knowledge.

To get started, please [**setup the learning environment**](environment) first.

