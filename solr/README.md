# Solr

This folder contains the materials of solr dataset.

To uncompress the folder, please use command:

```bash
tar -xJvf solr_data.tar.xz
```

To compress this folder, you may use command:

```bash
tar -cJvf solr_data.tar.xz drug_labels.csv drug_labels.json levetiracetam.xml
```

